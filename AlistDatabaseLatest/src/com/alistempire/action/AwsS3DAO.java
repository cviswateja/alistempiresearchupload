package com.alistempire.action;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.model.S3Object;
import com.opensymphony.xwork2.ActionSupport;
/**
 * @author Randy Alexander
 * Based on AWS S3 sample code
 * <p>
 * AWS S3 interface 
 */
	public class AwsS3DAO {
		String bucketName = "empire-social-media-tv";  // "Bucket" is similar to a directory
		AmazonS3 s3 = null;
		
	/*
	 * Credentials file must be present!!
	 * (C:\\Users\\Owner\\.aws\\credentials) where the  code will load the credentials from.
	 * <p>
	 * <b>WARNING:</b> To avoid accidental leakage of your credentials, DO NOT keep
	 * the credentials file in your source directory.
	 *
	 * http://docs.aws.amazon.com/AWSSdkDocsJava/latest/DeveloperGuide/credentials.html
	 * http://aws.amazon.com/security-credentials
	 */
	 //FIXME The credentials file location will need to change for server implementation.  Or embed credentials in this code

		
        /**
         * Constructor for the AWS S3 interface
         * <p>
         *
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (C:\\Users\\Owner\\.aws\\credentials).  //FIXME This file location will need to change for server implementation. Or embed credentials in this code
         */
	    public AwsS3DAO () {
    	
	        //AWSCredentials credentials = null;	//Use this when credentials are stored in the credentials file
	        BasicAWSCredentials credentials = null;	//Use this constructor for the alternate embedded method
	        try {
	            //credentials = new ProfileCredentialsProvider("default").getCredentials();	//Use this when credentials are stored in the credentials file
	            credentials = new BasicAWSCredentials("AKIAJRIVMSIPDUQ7QTFQ", "xMrTPE5yCCCVpZmbB8PUBWBidPteIc3v00dKIybu"); //Alternate embedded method
	        } catch (Exception e) {
	            throw new AmazonClientException(
	                    "Cannot load the credentials from the credential profiles file. " +
	                    "Please make sure that your credentials file is at the correct " +
	                    "location (C:\\Users\\Owner\\.aws\\credentials), and is in valid format.",
	                    e);
	        }

	        s3 = new AmazonS3Client(credentials);
	        Region usEast1 = Region.getRegion(Regions.US_EAST_1);
	        s3.setRegion(usEast1);
	    }

        /**
         * Upload an object to your bucket - You can easily upload a file to
         * S3, or upload directly an InputStream if you know the length of
         * the data in the stream. You can also specify your own metadata
         * when uploading to S3, which allows you set a variety of options
         * like content-type and content-encoding, plus additional metadata
         * specific to your applications.
         * <p>
         * @param key Name of the file
         * @param file actual file
         */
	    public void UploadS3 (String key, File file) {
	    		
	    	System.out.println("Uploading to S3 Starts");
	    	System.out.println("key is " +key);
	    	System.out.println("file is " +file);
	    	
	            //System.out.println("Uploading a new object to S3 from a file\n");
	            s3.putObject(new PutObjectRequest(bucketName, key, file));
	    }
	    
        /**
         * Download an object - When you download an object, you get all of
         * the object's metadata and a stream from which to read the contents.
         * It's important to read the contents of the stream as quickly as
         * possibly since the data is streamed directly from Amazon S3 and your
         * network connection will remain open until you read all the data or
         * close the input stream.
         *
         * GetObjectRequest also supports several other options, including
         * conditional downloading of objects based on modification times,
         * ETags, and selectively downloading a range of an object.
         * <p>
         * @param key Name of file
         * @return S3Object 
         */
	    public S3Object DownloadS3(String key) {
	    		//This should not be necessary as objects can be retrieved via URL
	    	S3Object object =null;
	    	try{
	            System.out.println("Downloading an object");
	            object = s3.getObject(new GetObjectRequest(bucketName, key));
	            return object;
	    	}
	        catch(Exception e){
	        	 return object;
	        }
	    }
	    
        /**
         * List objects in your bucket by prefix - There are many options for
         * listing the objects in your bucket.  Keep in mind that buckets with
         * many objects might truncate their results when listing their objects,
         * so be sure to check if the returned object listing is truncated, and
         * use the AmazonS3.listNextBatchOfObjects(...) operation to retrieve
         * additional results.
         * <p>
         * @return List<String> list of names of all files in S3 bucket
         */
	    public List<String> ListS3 () {
	    	
	            //System.out.println("Listing objects");
	            
	            List<String> list = new ArrayList<String>();
	            
	            ObjectListing objectListing = s3.listObjects(new ListObjectsRequest()
	                    .withBucketName(bucketName));
	            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
	                //System.out.println(objectSummary.getKey());
	                String object = new String(objectSummary.getKey());
	                list.add(object);
	            }
				return list;
	    }
	    
	    /**
	     * Deletes a file from S3
	     * <p>
	     * @param key
	     */
	    public void DeleteS3 (String key) {
	            /*
	             * Delete an object - Unless versioning has been turned on for your bucket,
	             * there is no way to undelete an object, so use caution when deleting objects.
	             */
	    	
	            //System.out.println("Deleting an object\n");
	            s3.deleteObject(bucketName, key);
	    }



	    /**
	     * Displays the contents of the specified input stream as text.
	     *
	     * @param input
	     *            The input stream to display as text.
	     *
	     * @throws IOException
	     */
	    private static void displayTextInputStream(InputStream input) throws IOException {
	        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
	        while (true) {
	            String line = reader.readLine();
	            if (line == null) break;

	            System.out.println("    " + line);
	        }
	    }

	}

	
