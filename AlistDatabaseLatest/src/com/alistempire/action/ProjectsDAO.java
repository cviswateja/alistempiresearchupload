package com.alistempire.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Randy Alexander
 *         <p>
 *         Database interface to the PostGres database Projectes Table
 */
public class ProjectsDAO {

	private Connection dbcon; // Connection

	/**
	 * Constructor Establishes a connection
	 */
	public ProjectsDAO() {

		String loginUser = "empire";
		String loginPasswd = "empire";
		// String loginUrl =
		// "jdbc:postgresql://54.86.174.143:5432/socialmediatv?searchpath=public
		// ";
		String loginUrl = "jdbc:mysql://104.197.178.191:3306/socialmediatv";
		// Load the PostgreSQL driver and connect
		try {
			// Class.forName("org.postgresql.Driver");
			Class.forName("com.mysql.jdbc.Driver");
			dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
		} catch (ClassNotFoundException ex) {
			System.err.println("ClassNotFoundException: " + ex.getMessage());

		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
		}
	}

	/**
	 * Inserts an object into the Porjects table
	 * <p>
	 * 
	 * @param Projects
	 *            object
	 */
	public void insertData(Projects projects) throws Exception {
		// Declare our statement
		PreparedStatement pst = null;

		// Postgres required mixcase table names to be in quotes.
		String stm = "INSERT INTO ";
		// stm+= "\"Projects\" ";
		stm += "Projects";
		stm += "(data, email, name, author, description, template, \"originalButterVersion\", \"latestButterVersion\", \"createdAt\", \"updatedAt\") ";
		stm += "VALUES(?,?,?,?,?,?,?,?,?,?)";

		// Perform the insert

		pst = dbcon.prepareStatement(stm);

		pst.setString(1, projects.getData());
		pst.setString(2, projects.getEmail());
		pst.setString(3, projects.getName());
		pst.setString(4, projects.getAuthor());
		pst.setString(5, projects.getDescription());
		pst.setString(6, "basic"); // Required Not Null
		pst.setString(7, "0.0.0.1"); // Required Not Null
		pst.setString(8, "0.0.0.1"); // Required Not Null
		pst.setTimestamp(9, getCurrentTimeStamp()); // Required Not Null
		pst.setTimestamp(10, getCurrentTimeStamp()); // Required Not Null

		pst.executeUpdate();

		pst.close();

	}

	/**
	 * Gets a timestamp for the current time
	 * <p>
	 * 
	 * @return java.sql.Timestamp
	 */
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}

	/**
	 * Returns a list of "Projects" objects Normally it should return only one
	 * object.
	 * <p>
	 * 
	 * @param input
	 *            name of the "Projects" object to return
	 * @return List<Projects> List of projects object
	 * @throws Exception
	 */
	public List<Projects> getProjectByName(String input) throws Exception {

		List<Projects> projectsList = new ArrayList<Projects>();

		try {
			// Declare our statement
			Statement statement = dbcon.createStatement();
			System.out.print(input);
			// Postgres required mixcase table names to be in quotes.
			String query = "SELECT * FROM ";
			// query+= "\"Projects\" ";
			query += "Projects";
			query += "where name=\'" + input + "\'";

			// Perform the query
			ResultSet rs = statement.executeQuery(query);

			// Iterate through each row of rs

			while (rs.next()) {
				Projects projects = new Projects();
				projects.setId(rs.getInt(1));
				projects.setData(rs.getString("data"));
				projects.setEmail(rs.getString("email"));
				projects.setName(rs.getString("name"));
				projects.setAuthor(rs.getString("author"));
				projects.setDescription(rs.getString("description"));
				// TODO Add the remaining columns

				projectsList.add(projects);
			}
			rs.close();
			statement.close();

		} catch (Exception e) {
			throw e;
		}
		return projectsList;
	}

	/**
	 * Returns a list of "Projects" objects
	 * 
	 * <p>
	 * 
	 * @param input
	 *            SQL query string
	 * @return List<Projects> List of projects object
	 * @throws Exception
	 */
	public List<Projects> sql(String input) throws Exception {
		List<Projects> projectsList = new ArrayList<Projects>();

		try {
			// Declare our statement
			Statement statement = dbcon.createStatement();

			// Input must be a valid SQL query that returns a project object
			// Postgres required mixcase table names to be in quotes.
			String query = input;

			// Perform the query
			ResultSet rs = statement.executeQuery(query);

			// Iterate through each row of rs
			// FIXME The result set may not match the Projects object
			while (rs.next()) {
				Projects projects = new Projects();
				projects.setId(rs.getInt("id"));
				projects.setData(rs.getString("data"));
				projects.setEmail(rs.getString("email"));
				projects.setName(rs.getString("name"));
				projects.setAuthor(rs.getString("author"));
				projects.setDescription(rs.getString("description"));
				// TODO Add the remaining columns

				projectsList.add(projects);
			}
			rs.close();
			statement.close();

		} catch (Exception e) {
			throw e;
		}
		return projectsList;
	}

	/**
	 * Returns a list of projects in the projects table
	 * <p>
	 * 
	 * @return List<String> List of file names
	 * @throws Exception
	 */
	public List<String> getAllProjectNames() throws Exception {
		// Output to String array
		List<String> projectsNames = new ArrayList<String>();

		try {
			// Declare our statement
			Statement statement = dbcon.createStatement();

			// Postgres required mixcase table names to be in quotes.
			String query = "SELECT name FROM ";
			// query+= "\"Projects\"";
			query += "Projects";
			// Perform the query
			ResultSet rs = statement.executeQuery(query);

			// Iterate through each row of rs

			while (rs.next()) {
				projectsNames.add(rs.getString("name"));
			}
			rs.close();
			statement.close();

		} catch (Exception e) {
			throw e;
		}
		return projectsNames;
	}
	// viswa added this method for signup functionality starts

	// Postgres required mixcase table names to be in quotes.

	// viswa added ends
	// viswa added to check the login credentials starts
	public Projects retrieveData(String email) throws Exception {
		System.out.print("Retrieve data starts");
		Projects loginCred = new Projects();
		// String username= projects.getName();
		// String password=projects.getPassword();
		try {
			Statement statement = dbcon.createStatement();
			String select = "select email,password from";
			// select+= "\"Account\" ";
			select += " Account";
			select += " where email='" + email + "'";
			System.out.println("query is " + select);
			ResultSet rel = statement.executeQuery(select);
			while (rel.next()) {
				loginCred.setEmail((rel.getString(1)));
				loginCred.setPassword((rel.getString(2)));
			}
		} catch (Exception e) {
			throw e;
		}
		return loginCred;
	}
	// viswa added above method for login check ends

	public String uploadVideo(uploadVideo projects) throws Exception {
		System.out.println("uploading Video method starts");
		try {
			PreparedStatement pst = null;

			String stm = "INSERT INTO ";
			// stm+= "\"Projects\" ";
			stm += " Projects";
			stm += "(data, email, name, author, description, template, originalButterVersion, latestButterVersion, createdAt, updatedAt, genre) ";
			stm += "VALUES(?,?,?,?,?,?,?,?,?,?,?)";

			// Perform the insert

			pst = dbcon.prepareStatement(stm);

			pst.setString(1, "");
			pst.setString(2, "");
			System.out.println("name is " + projects.getName());
			System.out.println("name2 is " + projects.getName2());
			// if(!projects.getName().matches(null)){
			if (projects.getName() != null) {
				System.out.println(projects.getName());
				pst.setString(3, projects.getName());
			}

			if (projects.getName2() != null) {
				System.out.println("String name is " + projects.getName2());
				pst.setString(3, projects.getName2());
			}
			if (projects.getName3() != null) {
				pst.setString(3, projects.getName3());
			}
			if (projects.getName4() != null) {
				pst.setString(3, projects.getName4());
			}
			if (projects.getAuthor() != null) {
				pst.setString(4, projects.getAuthor());
			}
			if (projects.getAuthor2() != null) {
				pst.setString(4, projects.getAuthor2());
			}
			if (projects.getAuthor3() != null) {
				pst.setString(4, projects.getAuthor3());
			}
			if (projects.getAuthor4() != null) {
				pst.setString(4, projects.getAuthor4());
			}
			if (projects.getTextDescription() != null) {
				pst.setString(5, projects.getTextDescription());
			}
			if (projects.getTextDescription2() != null) {
				pst.setString(5, projects.getTextDescription2());
			}
			if (projects.getTextDescription3() != null) {
				pst.setString(5, projects.getTextDescription3());
			}
			if (projects.getTextDescription4() != null) {
				pst.setString(5, projects.getTextDescription4());
			}
			pst.setString(6, "basic"); // Required Not Null
			pst.setString(7, "0.0.0.1"); // Required Not Null
			pst.setString(8, "0.0.0.1"); // Required Not Null
			pst.setTimestamp(9, getCurrentTimeStamp()); // Required Not Null
			pst.setTimestamp(10, getCurrentTimeStamp()); // Required Not Null
			if (projects.getGenre() != null) {
				pst.setString(11, projects.getGenre());
			}
			if (projects.getGenre2() != null) {
				pst.setString(11, projects.getGenre2());
			}
			if (projects.getGenre3() != null) {
				pst.setString(11, projects.getGenre3());
			}
			if (projects.getGenre4() != null) {
				pst.setString(11, projects.getGenre4());
			}
			System.out.println(pst);
			pst.executeUpdate();

			pst.close();
			return "success";
		} catch (Exception e) {
			return "error";
		}

	}

	/**
	 * Close the database connection
	 * 
	 * @throws SQLException
	 */
	public void closeDB() throws SQLException {
		try {
			dbcon.close();
		} catch (SQLException e) {
			throw e;

		}
	}

	// viswa added this method to implement search in local database -- starts
	public List<String> getNameFromDb(String input,String genre) throws Exception {
		List<Projects> projectsList = new ArrayList<Projects>();
		List<String> finalList = new ArrayList<String>();

		try {
			Statement statement = dbcon.createStatement();
			String query = "SELECT name FROM ";
			// query+= "\"Projects\" ";
			query += " Projects";
			query += " where name like '%" + input + "%' and genre like '%" + genre + "%'";
			ResultSet rs = statement.executeQuery(query);
			System.out.println(" The query is " + query);
			while (rs.next()) {
				String value = rs.getString("name");
				finalList.add(value);
			}
			rs.close();
			statement.close();
		} catch (Exception e) {
			throw e;
		}
		System.out.println("List is " + finalList);
		return finalList;
	}
	// viswa ends
	
	public List<String> getName(String input) throws Exception {
		List<Projects> projectsList = new ArrayList<Projects>();
		List<String> finalList = new ArrayList<String>();

		try {
			Statement statement = dbcon.createStatement();
			String query = "SELECT name FROM ";
			// query+= "\"Projects\" ";
			query += " Projects";
			query += " where name like '%" + input + "%'";
			ResultSet rs = statement.executeQuery(query);
			System.out.println(" The query is " + query);
			while (rs.next()) {
				String value = rs.getString("name");
				finalList.add(value);
			}
			rs.close();
			statement.close();
		} catch (Exception e) {
			throw e;
		}
		System.out.println("List is " + finalList);
		return finalList;
	}
	// viswa ends

	// Viswa added this method to get Author starts
	public List<String> getAuthorFromDb(String input,String genre) throws Exception {
		List<String> finalList = new ArrayList<String>();

		try {
			Statement statement = dbcon.createStatement();
			String query = "SELECT author FROM ";
			// query+= "\"Projects\" ";
			query += " Projects";
			query += " where author like '%" + input + "%' and genre like '%" + genre + "%'";
			ResultSet rs = statement.executeQuery(query);
			System.out.println(" The query is " + query);
			while (rs.next()) {
				String value = rs.getString("author");
				finalList.add(value);
			}
			rs.close();
			statement.close();
		} catch (Exception e) {
			throw e;
		}
		return finalList;
	}
	// Viswa ends
	
	// Viswa added this method to get Author starts
	public List<String> getAuthor(String input) throws Exception {
		List<String> finalList = new ArrayList<String>();

		try {
			Statement statement = dbcon.createStatement();
			String query = "SELECT author FROM ";
			// query+= "\"Projects\" ";
			query += " Projects";
			query += " where author like '%" + input + "%'";
			ResultSet rs = statement.executeQuery(query);
			System.out.println(" The query is " + query);
			while (rs.next()) {
				String value = rs.getString("author");
				finalList.add(value);
			}
			rs.close();
			statement.close();
		} catch (Exception e) {
			throw e;
		}
		return finalList;
	}
	// viswa added this method to get description starts
	public List<String> getDescFromDb(String input,String genre) throws Exception {
		List<Projects> projectsList = new ArrayList<Projects>();
		List<String> finalList = new ArrayList<String>();

		try {
			Statement statement = dbcon.createStatement();
			String query = "SELECT description FROM ";
			// query+= "\"Projects\" ";
			query += " Projects";
			query += " where description like '%" + input + "%' and genre like '%" + genre + "%'";
			ResultSet rs = statement.executeQuery(query);
			System.out.println(" The query is " + query);
			while (rs.next()) {
				String value = rs.getString("description");
				finalList.add(value);
			}
			rs.close();
			statement.close();
		} catch (Exception e) {
			throw e;
		}
		return finalList;
	}

	// viswa ends
	// viswa added this method to get description starts
	public List<String> getDesc(String input) throws Exception {
		List<Projects> projectsList = new ArrayList<Projects>();
		List<String> finalList = new ArrayList<String>();

		try {
			Statement statement = dbcon.createStatement();
			String query = "SELECT description FROM ";
			// query+= "\"Projects\" ";
			query += " Projects";
			query += " where description like '%" + input + "%'";
			ResultSet rs = statement.executeQuery(query);
			System.out.println(" The query is " + query);
			while (rs.next()) {
				String value = rs.getString("description");
				finalList.add(value);
			}
			rs.close();
			statement.close();
		} catch (Exception e) {
			throw e;
		}
		return finalList;
	}
	public String insertRecords(Credentials credentials) throws Exception {
		// Declare our statement
		Statement statement = dbcon.createStatement();
		System.out.println("insert records starts");
		// String emailUsed= projects.getEmail();
		PreparedStatement pst = null;
		String select = "select email from";
		select += " Account";
		select += " where username = '" + credentials.getName() + "'";
		System.out.println("query" + select);
		ResultSet rel = statement.executeQuery(select);
		if (!rel.next()) {
			String stm = "INSERT INTO ";
			stm += " Account";
			stm += "(email,first_name,last_name,password,username, status,creation_date, phone, dob)";
			stm += " VALUES(?,?,?,?,?,?,?,?,?)";
			pst = dbcon.prepareStatement(stm);
			pst.setString(1, credentials.getEmail());
			pst.setString(2, credentials.getFirstName());
			pst.setString(3, credentials.getLastName());
			pst.setString(4, credentials.getPassword());
			pst.setString(5, credentials.getName());
			pst.setString(6, "Active");
			pst.setTimestamp(7, getCurrentTimeStamp());
			if (credentials.getPhone() == null || credentials.getPhone() == "") {
				pst.setString(8, "null");
			} else {
				pst.setString(8, credentials.getPhone());
			}
			pst.setString(9, credentials.getDob());
			pst.executeUpdate();

			pst.close();
			return "success";
		} else {
			return "error";
		}
		// Postgres required mixcase table names to be in quotes.

	}
	// viswa added ends
}
