package com.alistempire.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.opensymphony.xwork2.ActionSupport;

/*
 * This is a test class to test the ProjectsDAO class
 */

public class ProjectsQuery extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Projects> projects = null;
	private String input;
	private List<String> result = new ArrayList<String>();

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public List<Projects> getProjects() {
		return projects;
	}

	public void setProjects(List<Projects> projects) {
		this.projects = projects;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String execute() {
		//get all projects names
		ProjectsDAO query = new ProjectsDAO();		
		try {
			
			result = query.getAllProjectNames();			
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}
	// viswa added this method for login check starts
		public String retrieval() throws Exception{  //get all projects names
			ProjectsDAO query = new ProjectsDAO();
			Projects projects= new Projects();
			try {
				String userid = email;
				String pwd= password;
				projects= query.retrieveData(userid);	
				if((userid.equals(projects.getEmail()) && pwd.equals(projects.getPassword())) || (userid.equalsIgnoreCase(projects.getEmail()) && pwd.equalsIgnoreCase(projects.getPassword()))) {
				return "success";
				}
				else{
					return "error";
				}
			}
				catch (Exception e) {
				addActionError(e.toString());
				return "error";
			} finally {
				try {
					query.closeDB();
				} catch (SQLException e) {
					addActionError(e.toString());
					return "error";
				}
			}
		}
		//Viswa added this method ends
	public String findName() {
		ProjectsDAO query = new ProjectsDAO();
		try {			
			projects = query.getProjectByName(input);
			Projects project = projects.get(0);
			System.out.println(projects.get(0));
			result.add(project.toString());
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}

	}

	public String sql() {
		ProjectsDAO query = new ProjectsDAO();
		try {
			projects = query.sql(input);
			Projects project = projects.get(0);
			result.add(project.toString());
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}
	
	public String insert() {
		ProjectsDAO insert = new ProjectsDAO();
		
		//Test Data
		Projects data = new Projects();
		data.setData("Test Data");
		data.setEmail("test@alist.com");
		data.setName("Test Name");
		data.setAuthor("Test Author");
		data.setDescription("Test Description");
		
		try {
			insert.insertData(data);
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				insert.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}
	
	private String email;
	private String password;
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	// viswa added above method for login check ends
	
}