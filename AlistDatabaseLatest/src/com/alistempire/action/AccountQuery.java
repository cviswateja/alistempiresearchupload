package com.alistempire.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

/*
 * This is a test class to test the AccountDAO class
 */

public class AccountQuery extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Account> Account = null;
	private String input;
	private List<String> result = new ArrayList<String>();

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public List<Account> getAccount() {
		return Account;
	}

	public void setAccount(List<Account> Account) {
		this.Account = Account;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String execute() {  //get all Account names
		AccountDAO query = new AccountDAO();		
		try {
			result = query.getAllUsernames();			
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}

	public String findName() {
		AccountDAO query = new AccountDAO();
		try {			
			Account = query.getAccountByName(input);
			Account project = Account.get(0);
			result.add(project.toString());
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}

	}
	
	public String findPassword() {
		AccountDAO query = new AccountDAO();
		try {			
			String password = query.getPassword(input);
			result.add(password);
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}

	}

	public String sql() {
		AccountDAO query = new AccountDAO();
		try {
			Account = query.sql(input);
			Account project = Account.get(0);
			result.add(project.toString());
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}
	
	public String insert() {
		AccountDAO insert = new AccountDAO();
		
		//Test Data
		Account data = new Account();
		data.setEmail("Test@test.com");
		data.setFirst_name("TestFirstName");
		data.setLast_name("TestLastName");
		data.setUsername("TestUsername");
		data.setPassword("TestPW");
		data.setStatus("TestStatus");
		
		try {
			insert.insertData(data);
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				insert.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}
	public String insertData() {
		AccountDAO insert = new AccountDAO();
		
		//Test Data
		Account data = new Account();
		data.setEmail("Test@test.com");
		data.setFirst_name("TestFirstName");
		data.setLast_name("TestLastName");
		data.setUsername("TestUsername");
		data.setPassword("TestPW");
		data.setStatus("TestStatus");
		
		try {
			insert.insertData(data);
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				insert.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}

	
}