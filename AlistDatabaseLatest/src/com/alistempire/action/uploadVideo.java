package com.alistempire.action;
import java.io.File;
import org.apache.commons.io.FileUtils;
import java.io.IOException; 

import com.opensymphony.xwork2.ActionSupport;

public class uploadVideo extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public uploadVideo(){
		
	}
	
	// Viswa added for sign up check starts
	public String execute() throws Exception{
		ProjectsDAO insert = new ProjectsDAO();
			AwsS3DAO aws= new AwsS3DAO();
			try {
				String output= insert.uploadVideo(this); 
				
				if(name!=null && output.equals("success") ){
					aws.UploadS3(name, myFile);
				}
				 if(name2!=null && output.equals("success")){
					aws.UploadS3(name2, myFile);
				}
				if(name3!=null && output.equals("success")){
					aws.UploadS3(name3, myFile);
				}
			if(name4!=null && output.equals("success")){
					aws.UploadS3(name4, myFile);
				}
				return "success";
			} catch (Exception e) {
				addActionError(e.toString());
				return "error";
			}
		}
		// method ends

	private String name;

	private String author;
	public String getAuthor2() {
		return author2;
	}
	public void setAuthor2(String author2) {
		this.author2 = author2;
	}
	public String getAuthor3() {
		return author3;
	}
	public void setAuthor3(String author3) {
		this.author3 = author3;
	}
	public String getAuthor4() {
		return author4;
	}
	public void setAuthor4(String author4) {
		this.author4 = author4;
	}
	

	private String author2;
	private String author3;
	private String author4;
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getTextDescription() {
		return textDescription;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

	public String getTextDescription2() {
		return textDescription2;
	}

	public void setTextDescription2(String textDescription2) {
		this.textDescription2 = textDescription2;
	}

	public String getTextDescription3() {
		return textDescription3;
	}

	public void setTextDescription3(String textDescription3) {
		this.textDescription3 = textDescription3;
	}

	public String getTextDescription4() {
		return textDescription4;
	}

	public void setTextDescription4(String textDescription4) {
		this.textDescription4 = textDescription4;
	}
	private String genre;
	private String genre2;
	public String getGenre2() {
		return genre2;
	}

	public void setGenre2(String genre2) {
		this.genre2 = genre2;
	}

	public String getGenre3() {
		return genre3;
	}

	public void setGenre3(String genre3) {
		this.genre3 = genre3;
	}

	public String getGenre4() {
		return genre4;
	}

	public void setGenre4(String genre4) {
		this.genre4 = genre4;
	}

	private String genre3;
	private String genre4;
	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	private String name2;
	private String name3;
	private String name4;
	private String textDescription;
	private String textDescription2;
	private String textDescription3;
	private String textDescription4;
	private File myFile;
	public File getMyFile() {
		return myFile;
	}
	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}


}
