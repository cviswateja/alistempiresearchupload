package com.alistempire.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.model.S3Object;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Test Class to test AWS S3 operations.
 */
public class S3Query extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String input;
	private List<Projects> projects = null;

	// Viswa added these methods starts
	private List<String> finalResult = null;

	private List<Projects> totalOutput = null;

	private String fileToUpload;

	public String getFileToUpload() {
		return fileToUpload;
	}

	public void setFileToUpload(String fileToUpload) {
		this.fileToUpload = fileToUpload;
	}

	public List<String> getFinalResult() {
		return finalResult;
	}

	public void setFinalResult(List<String> finalResult) {
		this.finalResult = finalResult;
	}

	public List<Projects> getTotalOutput() {
		return totalOutput;
	}

	public void setTotalOutput(List<Projects> totalOutput) {
		this.totalOutput = totalOutput;
	}

	// Viswa added these methods ends
	public List<Projects> getProjects() {
		return projects;
	}

	public void setProjects(List<Projects> projects) {
		this.projects = projects;
	}

	private List<String> result = new ArrayList<String>();

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	// viswa added starts
	private String searchLocal;
	private String searchBy;
	private String searchByValue;
	public String getSearchByValue() {
		return searchByValue;
	}

	public void setSearchByValue(String searchByValue) {
		this.searchByValue = searchByValue;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	private String author;
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getTextDescription() {
		return textDescription;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

	public String getTextDescription2() {
		return textDescription2;
	}

	public void setTextDescription2(String textDescription2) {
		this.textDescription2 = textDescription2;
	}

	public String getTextDescription3() {
		return textDescription3;
	}

	public void setTextDescription3(String textDescription3) {
		this.textDescription3 = textDescription3;
	}

	public String getTextDescription4() {
		return textDescription4;
	}

	public void setTextDescription4(String textDescription4) {
		this.textDescription4 = textDescription4;
	}

	

	private String name;
	private String name2;
	private String name3;
	private String name4;
	private String textDescription;
	private String textDescription2;
	private String textDescription3;
	private String textDescription4;
	private File file;

	public void setFile(File file) {
		this.file = file;
	}

	public String getSearchLocal() {
		return searchLocal;
	}

	public void setSearchLocal(String searchLocal) {
		this.searchLocal = searchLocal;
	}
	// viswa added ends

	public String listS3() { // Get List of files in S3
		AwsS3DAO query = new AwsS3DAO();
		try {
			result = query.ListS3();
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		}
	}

	public String getFile() { // Get a file from S3
		// This is not really necessary as S3 files can be accessed with at URL.
		AwsS3DAO query = new AwsS3DAO();
		try {
			S3Object object = query.DownloadS3(input);
			System.out.print(object);
			result.add(object.toString());
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		}

	}

	public String putFile() { // Put a file into S3
		AwsS3DAO insert = new AwsS3DAO();
		String key = new String();// FIXME
		File file = null;// FIXME
		try {
			// insert.UploadS3(key, file);
			insert.UploadS3(fileToUpload, createSampleFile());
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		}
	}

	public String deleteFile() { // Delete a S3 file
		AwsS3DAO query = new AwsS3DAO();
		try {
			query.DeleteS3(input);
			return "success";
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		}

	}

	/**
	 * Creates a temporary file with text data to demonstrate uploading a file
	 * to Amazon S3
	 *
	 * @return A newly created temporary file with text data.
	 *
	 * @throws IOException
	 */
	private static File createSampleFile() throws IOException {
		File file = File.createTempFile("aws-java-sdk-", ".txt");
		file.deleteOnExit();

		Writer writer = new OutputStreamWriter(new FileOutputStream(file));
		writer.write("abcdefghijklmnopqrstuvwxyz\n");
		writer.write("01234567890112345678901234\n");
		writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
		writer.write("01234567890112345678901234\n");
		writer.write("abcdefghijklmnopqrstuvwxyz\n");
		writer.close();

		return file;
	}

	// Viswa added this method to search local in our database starts
	public String searchLocal() {
		System.out.println("searchlocal method starts");
		ProjectsDAO query = new ProjectsDAO();
		AwsS3DAO aws = new AwsS3DAO();
		try {
			System.out.println("ghas" +searchBy);
			System.out.println("searchbyvalue" +searchByValue);
			if(searchBy.equals("Name") && searchByValue!=null){
			finalResult = query.getNameFromDb(searchLocal,searchByValue);
			}
			if(searchBy.equals("Name") && searchByValue==null){
				finalResult = query.getName(searchLocal);
				}
			if(searchBy.equals("Author") && searchByValue!=null){
				finalResult=query.getAuthorFromDb(searchLocal,searchByValue);
			}
			if(searchBy.equals("Description") && searchByValue!=null){
				finalResult=query.getDescFromDb(searchLocal,searchByValue);
			}
			if(searchBy.equals("Author") && searchByValue==null){
				finalResult=query.getAuthor(searchLocal);
			}
			if(searchBy.equals("Description") && searchByValue==null){
				finalResult=query.getDesc(searchLocal);
			}
			//String value = "http://empire-social-media-tv.s3.amazonaws.com/";
			int size = finalResult.size();
			System.out.println("the list is " + size);
			if (size == 0) {
				finalResult.add("There are no local videos available.");
				// finalResult.add("king");
				return "success";
			} else {
				for (int i = 0; i < size; i++) {
					System.out.println("the elemets are " + finalResult.get(i));
					String value1 = finalResult.get(i);
					S3Object object = aws.DownloadS3(value1);
					String key = object.getKey();
					finalResult.add(key);
					System.out.println("size is " + finalResult.get(0));
					finalResult.remove(i);
				}
				return "success";
			}
		} catch (Exception e) {
			addActionError(e.toString());
			return "error";
		} finally {
			try {
				query.closeDB();
			} catch (SQLException e) {
				addActionError(e.toString());
				return "error";
			}
		}
	}
	
	// method ends

}
