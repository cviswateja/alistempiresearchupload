package com.alistempire.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Randy Alexander
 * <p>
 * Database interface to the PostGres database Account Table
 */
public class AccountDAO {

	private Connection dbcon;  // Connection 

	/**
	 * Constructor
	 * Establishes a connection
	 */
	public AccountDAO(){

		String loginUser = "empire";
		String loginPasswd = "empire";
		//String loginUrl = "jdbc:postgresql://54.86.174.143:5432/socialmediatv?searchpath=public ";
		String loginUrl = "jdbc:mysql://104.197.178.191:3306/socialmediatv";
		// Load the PostgreSQL driver and connect
		try 
		{
			//Class.forName("org.postgresql.Driver");
			Class.forName("com.mysql.jdbc.Driver");
			dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
		}
		catch (ClassNotFoundException ex)
		{
			System.err.println("ClassNotFoundException: " + ex.getMessage());

		}
		catch (SQLException ex)
		{
			System.err.println("SQLException: " + ex.getMessage());
		}
	}	

/**
 * Inserts an object into the Porjects table
 * <p>
 * @param Account object
 */
	public void  insertData(Account account) throws Exception
	{
		// Declare our statement
		PreparedStatement  pst = null;
		 

		//Postgres required mixcase table names to be in quotes.
		String stm = "INSERT INTO ";
		//stm+= "\"Account\" ";
		stm+="Account";
		stm+= "(email, first_name, last_name, password, username, status, creation_date) ";
		stm+= "VALUES(?,?,?,?,?,?,?)";

		// Perform the insert
		
		pst = dbcon.prepareStatement(stm);
		
		pst.setString(1, account.getEmail());
		pst.setString(2, account.getFirst_name());
		pst.setString(3, account.getLast_name());
		pst.setString(4, account.getPassword());
		pst.setString(5, account.getUsername());
		pst.setString(6, account.getStatus());
		pst.setTimestamp(7, getCurrentTimeStamp());
		pst.executeUpdate();
	
		pst.close();

	}
	
	/**
	 * Gets a timestamp for the current time
	 * <p>
	 * @return java.sql.Timestamp
	 */
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}

/**
 * Returns a list of usernames in the account table
 * <p>
 * @return List<String>  List of file names
 * @throws Exception
 */
	public List<String> getAllUsernames() throws Exception 
	{
		// Output to String array
		List<String> userNames = new ArrayList<String>();

		try
		{
			// Declare our statement
			Statement statement = dbcon.createStatement();

			//Postgres required mixcase table names to be in quotes.
			String query = "SELECT username FROM ";
			//query+= "\"Account\"";
			query+= "Account";

			// Perform the query
			ResultSet rs = statement.executeQuery(query);

			// Iterate through each row of rs
			
			while (rs.next())
			{
				userNames.add(rs.getString("name"));
			}
			rs.close();
			statement.close();

		}
		catch(Exception e){
			throw e;
		}
		return userNames;
	}

	/**
	 * Returns a list of "Account" objects
	 * Normally it should return only one object.
	 * <p>
	 * @param username of the "Account" object to return
	 * @return List<Account> List of account object
	 * @throws Exception
	 */
	public List<Account> getAccountByName(String input) throws Exception 
	{

		List<Account> accountList = new ArrayList<Account>();

		try
		{
			// Declare our statement
			Statement statement = dbcon.createStatement();

			//Postgres required mixcase table names to be in quotes.
			String query = "SELECT * FROM ";
			//query+= "\"Account\" ";
			query+= "Account";
			query+= "where username=\'" + input + "\'";

			// Perform the query
			ResultSet rs = statement.executeQuery(query);

			// Iterate through each row of rs

			while (rs.next())
			{
				Account account = new Account();
				account.setAccount_id(rs.getInt(1));
				account.setEmail(rs.getString("email"));
				account.setFirst_name(rs.getString("first_name"));
				account.setLast_name(rs.getString("last_name"));
				account.setPassword(rs.getString("password"));
				account.setUsername(rs.getString("username"));
				account.setStatus(rs.getString("status"));
				account.setCreation_date(rs.getDate("creation_date"));
				
				accountList.add(account);
			}
			rs.close();
			statement.close();

		}
		catch(Exception e){
			throw e;
		}
		return accountList;
	}

	/**
	 * Returns a password for a username
	 * 
	 * <p>
	 * @param username
	 * @return String password, null if does not exist
	 * @throws Exception
	 */
	public String getPassword(String input) throws Exception 
	{
		String password = new String();

		try
		{
			// Declare our statement
			Statement statement = dbcon.createStatement();

			//Postgres required mixcase table names to be in quotes.
			String query = "SELECT password FROM ";
			//query+= "\"Account\" ";
			query+= "Account";
			query+= "where username=\'" + input + "\'";

			// Perform the query
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) { //Should only be one
				try {
					password = rs.getString("password");
				} catch (Exception e) {
					password = null;
				} 
			}
			
			rs.close();
			statement.close();

		}
		catch(Exception e){
			throw e;
		}
		return password;
	}

	
	
	/**
	 * Returns a list of "Account" objects
	 * 
	 * <p>
	 * @param input SQL query string
	 * @return List<Account> List of account object
	 * @throws Exception
	 */
	public List<Account> sql(String input) throws Exception 
	{
		List<Account> accountList = new ArrayList<Account>();

		try
		{
			// Declare our statement
			Statement statement = dbcon.createStatement();

			//Input must be a valid SQL query that returns a project object
			//Postgres required mixcase table names to be in quotes.
			String query = input;

			// Perform the query
			ResultSet rs = statement.executeQuery(query);

			// Iterate through each row of rs
			//FIXME  The result set may not match the Accounts object
			while (rs.next())
			{
				Account account = new Account();
				account.setAccount_id(rs.getInt(1));
				account.setEmail(rs.getString("email"));
				account.setFirst_name(rs.getString("first_name"));
				account.setLast_name(rs.getString("last_name"));
				account.setPassword(rs.getString("password"));
				account.setUsername(rs.getString("username"));
				account.setStatus(rs.getString("status"));
				account.setCreation_date(rs.getDate("creation_date"));
				
				accountList.add(account);
			}
			rs.close();
			statement.close();

		}
		catch(Exception e){
			throw e;
		}
		return accountList;
	}


/**
 * Close the database connection
 * 
 * @throws SQLException
 */
	public void closeDB() throws SQLException {
		try {
			dbcon.close();
		} catch (SQLException e) {
			throw e;

		}
	}

}
