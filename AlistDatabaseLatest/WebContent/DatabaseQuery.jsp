<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Query Media</title>
</head>
<body>
<h2> Test Application</h2>

<s:actionerror />
<s:form action="Query" method="post">
    <s:textfield name="input" label="Query" size="20" />
    <s:submit value="Find All" align="center" />
    <s:submit action="findname" value="Find Name" align="center" />
	<s:submit action="sql" value="SQL Statement" align="center" />
	<s:submit action="insert" value="Test Insert" align="center" />
	<s:submit action="listS3" value="List S3" align="center" />
	<s:submit action="getS3" value="Get S3" align="center" />
	<s:submit action="deleteS3" value="Delete S3" align="center" />
	<s:submit action="getPassword" value="Get Password" align="center" />
</s:form>
</body>
</html>