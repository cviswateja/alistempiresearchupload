<!-- @Author: This File has been added by Viswa for local search and youtube search functionalites -->
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Welcome</title>
</head>

<body onload="keyWordsearch()">
 <script>
    function keyWordsearch(){
        gapi.client.setApiKey('AIzaSyBXjh4UnMm1dFrgkdKPnWb8CRoOsILv7_Y');
            gapi.client.load('youtube', 'v3', function() {
                    makeRequest();
            });
    }
 function makeRequest(){
	var q= '<s:property value="searchLocal"/>';
	 var request = gapi.client.youtube.search.list({
         q: q,
         part: 'snippet'                        
     });
     request.execute(function(response){
         var str = JSON.stringify(response.result,'',4);
         $('#search-container').val( str);
         makeControl(response);
     });
 }
 
 function makeControl(resp){
	 var stList = '<table id="res1" border="1" cellspacing="1" width="100%"><tbody>'; 
     for (var i=0; i<resp.items.length;i++){
         var vid = resp.items[i].id.videoId; 
         var tit = resp.items[i].snippet.title;
         if(typeof vid != 'undefined'){    
             stList += '<tr><td style="width:80px;vertical-align:top">'+
               '<a class="show" href="#" title="'+ vid + '" onclick="playVid(this);'+
               ' return false">'+
               '<img  width="80" height="60" src="http://img.youtube.com/vi/'+ 
               vid +'/default.jpg"></a></td>'+
               '<td><b>'+i+'</b>-'+ tit +'</td></tr>';
         }
     }
     document.getElementById('list1').innerHTML = stList + '</tbody></table>';
 }
 
 function playVid(thi){
     var st = 'https://www.youtube.com/embed/'+thi.title+'?autoplay=1';
     document.getElementById('player').src = st; 
 }
 
 </script>

 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
 <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"> </script>


    <h2>Results: 
   <%--  <s:property value="searchLocal"/> --%>
  <s:iterator id="myVariable" value="finalResult">
  <s:if test="#myVariable == 'There are no local videos available.'">
 	There are no local videos available.
  </s:if>
  <s:else>
  <b>URL for Videos:</b>
  <a href="<s:url value='http://empire-social-media-tv.s3.amazonaws.com/%{myVariable}' />" target="_blank"><s:property value="myVariable" /></a>
  </s:else>
</s:iterator>
<div id="list1" 
     style="width:853px;height:400px;overflow:auto;background-color:#EEEEEE">
     </div>
 <iframe id="player" width="853" height="480" src="" frameborder="1" allowfullscreen>
 </iframe>
 </body>
</html>